const startUrl = 'https://rickandmortyapi.com/api/character';
window.onscroll = function(ev) {
    if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
        loadCharacters();
    }
};

function loadCharacters(){
    const characters = document.getElementById('characters');
    const count = characters.childElementCount;
    let url;
    if(count < 10 || localStorage.getItem('next') === null){
        url = startUrl; 
    }
    else{
        url = localStorage.getItem('next');
    }

    fetch(url)
        .then(res => res.json())
        .then(data => {
            const rawData = data.results;
            localStorage.setItem('next', data.info.next);
            return rawData.map(character => {
            let card = createCard(character);
            characters.appendChild(card);
            });
        })
        .then(()=>{
            getSelectedValue();
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });
    }

function createCard(character){
    let card = createElement('div', 'card');
    let id = createElement('div', 'hidden', character.id);
    card.appendChild(id);
    let imageBlock = createElement('div', 'image')
    let img = createImage(character.image);
    imageBlock.appendChild(img);
    card.appendChild(imageBlock);
    let nameDiv = createElement('div', 'name', '<span>Name:</span> ' + character.name);
    card.appendChild(nameDiv);
    let speciesDiv = createElement('div', 'species', '<span>Species:</span> ' + character.species);
    card.appendChild(speciesDiv);
    let locationDiv = createElement('div', 'location', '<span>Location:</span> ' + character.location.name);
    card.appendChild(locationDiv);
    let createdDiv = createElement('div', 'created', '<span>Created:</span> ' + character.created.substr(0,10));
    card.appendChild(createdDiv);
    let episodeDiv = createElement('div', 'episode', '<span>Episodes:</span> ' + character.episode.length);
    card.appendChild(episodeDiv);
    let deleteBlock= createElement('div', 'delete', '<i class="fa fa-trash fa-1.75x"></i>')
    deleteBlock.addEventListener("click", removeOnClick);
    card.appendChild(deleteBlock);
    return card;
}

function createElement(tagName, className, value = ''){
    let element = document.createElement(tagName);
    element.setAttribute('class', className);
    element.innerHTML = value;
    return element;
}

function createImage(src){
    let img = document.createElement('img');
    img.src = src;
    return img;
}

function removeOnClick(event){
    let clickedElement = event.target.parentElement.parentElement;
    clickedElement.parentElement.removeChild(clickedElement);
}

function getSelectedValue(){
    let value = document.getElementById("filters").value;
    const characters = document.getElementById('characters').children;
    let sortFunc;
    if(value === "ID"){
        sortFunc = function(a,b) {
            return a.firstChild.innerText - b.firstChild.innerText;
        }
    }
    else if(value === "DateASC"){
        sortFunc = function(a,b) {
            return a.children[5].innerText.substr(9).localeCompare(b.children[5].innerText.substr(9));
        }
    }
    else if(value === "DateDESC"){
        sortFunc = function(a,b) {
            return b.children[5].innerText.substr(9).localeCompare(a.children[5].innerText.substr(9));
        }
    }
    else {
        sortFunc = function(a,b) {
            return b.children[6].innerText.substr(10) - a.children[6].innerText.substr(10);
        }
    }
    let items = Array.prototype.slice.call(characters);
    items.sort(sortFunc);
    for(var i = 0, len = items.length; i < len; i++) {
        var parent = items[i].parentNode;
        var detatchedItem = parent.removeChild(items[i]);
        parent.appendChild(detatchedItem);
    }
}